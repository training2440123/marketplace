from flask import Blueprint

from app import Api
from resources import UserResource

user_route = Blueprint("user_route", __name__)

api = Api(user_route)
api.add_resource(
    UserResource,
    f"/{UserResource.__endpoint__}",
    endpoint=UserResource.__endpoint__,
)
