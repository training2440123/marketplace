from flask import Blueprint

from app import Api
from resources import AuthenticationsResource

user_authenticate_route = Blueprint("user_authenticate_route", __name__, url_prefix="/users")


api = Api(user_authenticate_route)
api.add_resource(
    AuthenticationsResource,
    f"/{AuthenticationsResource.__endpoint__}",
    endpoint=AuthenticationsResource.__endpoint__,
)
