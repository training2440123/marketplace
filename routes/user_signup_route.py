from flask import Blueprint

from app import Api
from resources import UserSignupResource

user_signup_route = Blueprint("user_signup_route", __name__, url_prefix="/users")

api = Api(user_signup_route)
api.add_resource(
    UserSignupResource,
    f"/{UserSignupResource.__endpoint__}",
    endpoint=UserSignupResource.__endpoint__,
)
