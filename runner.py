from flask import Blueprint
from flask_apispec.views import MethodResourceMeta

from routes import *
from models import *
from documents import *

from app import create_app, app, docs
from models.base_model import Base, engine

app = create_app(app)

with app.app_context():
    Base.metadata.create_all(engine)

"""Loop through and register all blueprint"""
_ = [
    app.register_blueprint(blueprint)
    for blueprint in globals().values()
    if isinstance(blueprint, Blueprint)
]

"""Loop through and register all swagger docuement"""
_ = [
    docs.register(
        document,
        endpoint=document.__endpoint__,
        blueprint=document.__blueprint__,
    )
    for document in globals().values()
    if issubclass(document.__class__, MethodResourceMeta)
]

print(app.url_map)

if __name__ == "__main__":
    app.run("0.0.0.0", port=5000)
