FROM python:3.11 as BASE

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Keeps Python from generating
# .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

WORKDIR /api

COPY . ./

RUN pip install -r requirements.txt

FROM BASE as PRODUCTION

FROM BASE as DEVELOPMENT

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]