import flask_restful
from apispec import APISpec

from flask import Flask, make_response
from apispec.ext.marshmallow import MarshmallowPlugin
from flask_apispec.extension import FlaskApiSpec

from .config import DevelopmentConfigs


class Api(flask_restful.Api):
    pass


class Resource(flask_restful.Resource):
    __endpoint__ = ""


app = Flask(__name__)

app.config.update(
    {
        "APISPEC_SPEC": APISpec(
            title="KlangDev Documentation",
            version="1.0.0",
            openapi_version="2.0",
            plugins=[MarshmallowPlugin()],
        ),
        "APISPEC_SWAGGER_UI_URL": "/documents/",
        "APISPEC_SWAGGER_URL": "/json/",
    }
)


docs = FlaskApiSpec(app)


def create_app(app, config: object = DevelopmentConfigs):
    app.config.from_object(config)

    return app
