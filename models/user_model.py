from sqlalchemy import Column, String

from schemas import UserSchema

from .base_model import BaseModel, Base


class UserModel(BaseModel, Base):
    __tablename__ = "user"
    __schema__ = UserSchema

    first_name = Column(String)
    last_name = Column(String)

    __phone = Column("phone", String(15))
    __password = Column("password", String(256))
