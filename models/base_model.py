from sqlalchemy import create_engine, Column, Integer, DateTime, func, desc, asc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker, Query


engine = create_engine("sqlite:///db.sql")

session = scoped_session(sessionmaker(engine))

Base = declarative_base()


class BaseModel:
    id = Column(Integer, primary_key=True, autoincrement=True)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, onupdate=func.now())

    _session = session

    def __init__(self, schema) -> None:

        self.__pluralname__ = f"{self.__class__.__name__.lower().replace('model', '')}s"

        for key, value in schema.items():
            if hasattr(self, key):
                setattr(self, key, value)

    def get_query(self, sort=None, sort_key="id", **expression):

        query = self._session.query(self.__class__).filter_by(**expression)

        sort_attr = getattr(self.__class__, sort_key)

        return (
            query
            if sort is None
            else query.order_by(desc(sort_attr) if sort == "desc" else asc(sort_attr))
        )

    def get_all(self, sort=None, sort_key="id", **expression):
        self.__models__ = self.get_query(
            sort=sort, sort_key=sort_key, **expression
        ).all()

        return self.__models__

    def add(self):
        self._session.add(self)
        self._session.commit()

    def jsonify(self, ikwargs: dict = dict(), dkwargs: dict = dict()):
        """{many: True}"""
        schema = self.__schema__(**ikwargs)

        json = schema.dump(
            self.__models__ if ikwargs.get("many", False) else self, **dkwargs
        )

        return {f"{self.__pluralname__}": json} if isinstance(json, list) else json
