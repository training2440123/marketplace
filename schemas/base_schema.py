from marshmallow import Schema, pre_load, post_dump, fields
from stringcase import snakecase, camelcase


class ExcludeSchema(Schema):
    class Meta:
        exlude = "UNKOWN"


class JsMix(Schema):
    @pre_load
    def to_snakecase(self, data, **kwargs):

        _data = {snakecase(k): v for k, v in data.items()}

        return _data

    @post_dump
    def to_camelcase(self, data, **kwargs):
        _data = {camelcase(key): value for key, value in data.items()}
        return _data


class BaseSchema(ExcludeSchema, JsMix):
    created_at = fields.DateTime(dump_only=True)
    updated_at = fields.DateTime(dump_only=True)
