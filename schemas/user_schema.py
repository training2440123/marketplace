from marshmallow import fields, validate

from utils.commons import validate_phone

from .base_schema import BaseSchema


class UserSchema(BaseSchema):
    first_name = fields.Str(validate=validate.Length(min=3, max=128))
    last_name = fields.Str(validate=validate.Length(min=3, max=128))
    gender = fields.Str(validate=validate.OneOf(["Male", "Female", "Other"]), required=True)
    phone = fields.Str(validate=validate_phone)
