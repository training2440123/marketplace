from app import Resource


class UserSignupResource(Resource):
    __endpoint__ = "signup"

    def get():
        return "Signup"
