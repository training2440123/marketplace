from flask import make_response

from app import Resource
from models import UserModel
from schemas import UserSchema

from utils.decorators import request_validate


class UserResource(Resource):
    __endpoint__ = "users"

    method_decorators = {
        "get": [request_validate(UserSchema, many=True)],
        "post": [request_validate(UserSchema, many=True)],
    }

    def get(self):
        return make_response({"message": "Hello User"})

    def post(self, user: dict = None):

        user_model = UserModel(user)

        user_model.get_all(id=2)

        return make_response(user_model.jsonify({"many": True}), 200)
