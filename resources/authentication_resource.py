from flask import make_response

from app import Resource


class AuthenticationsResource(Resource):
    __endpoint__ = "authenticate"

    def get(self):
        return make_response({"message": "Hello"}, 200)
