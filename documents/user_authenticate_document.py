from flask_apispec.views import MethodResource
from flask_apispec import marshal_with, use_kwargs, doc

from schemas import UserSchema
from utils.commons import create_schema


@doc(tags=["Authenticate"])
class UserAuthenticateDocument(MethodResource):
    __endpoint__ = "authenticate"
    __blueprint__ = "user_authenticate_route"

    def get(self):
        pass
