from flask_apispec.views import MethodResource
from flask_apispec import marshal_with, use_kwargs, doc

from schemas import UserSchema
from utils.commons import create_schema


User = create_schema("User", UserSchema)
Paginate = create_schema("Pagination", UserSchema)
CreateUser = create_schema("UserCreate", UserSchema)



class UserDocument(MethodResource):
    __endpoint__ = "users"
    __blueprint__ = "user_route"

    @doc(tags=["User"], description="Hello")
    @marshal_with(User(), description="User default response")
    @marshal_with(Paginate(), code=200, description="Paginate response")
    @use_kwargs(User(partial=True), location=("query"))
    def get(self):
        pass

    @doc(tags=["User"], description="Hello")
    @use_kwargs(CreateUser(only=["first_name", "last_name"]), description="Create user")
    @marshal_with(User, code=200)
    def post(self):
        pass
