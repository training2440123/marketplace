from stringcase import camelcase
from marshmallow.fields import Field


def validate_phone(phone: str):
    pass


def create_schema(name: str, cls: type, opt: dict = {}):

    @classmethod
    def on_bind_field(self, field_name: str, field_obj: Field, _cc=camelcase) -> None:
        field_obj.data_key = _cc(field_obj.data_key or field_name)
        field_obj.dump_only = False

    _cls = type(name, (cls,), opt)

    _cls.on_bind_field = on_bind_field

    return _cls
