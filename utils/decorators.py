from functools import wraps

from flask import request, abort
from marshmallow.exceptions import ValidationError


def request_validate(Schema, **mar_kwargs):

    def decorator(func):

        data = request.json if func.__name__ != "get" else request.args.to_dict()

        schema = Schema()

        try:

            data_dict = schema.load(data)

        except ValidationError as err:
            print(err)
            abort(400)
            """
                TODO:
                    1) should implment login so we track error on production
                    2) we should error to communication channel so we easier to debug
                    3) ....
            """

        @wraps(func)
        def inner(*args, **kwargs):

            return func(data_dict, *args, **kwargs)

        return inner

    return decorator
